import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApisService {
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getGateways(): any {
    const url = `${this.baseUrl}/gateways`;
    return this.http.get(url);
  }

  addGateway(payload): any {
    const url = `${this.baseUrl}/gateways`;
    return this.http.post(url, payload);
  }

  getDevices(gatewayId): any {
    const url = `${this.baseUrl}/devices/${gatewayId}`;
    return this.http.get(url);
  }

  addDevice(payload): any {
    const url = `${this.baseUrl}/devices`;
    return this.http.post(url, payload);
  }

  removeDevice(id): any {
    const url = `${this.baseUrl}/devices/${id}`;
    return this.http.delete(url);
  }

}
