import {Component, OnDestroy, OnInit} from '@angular/core';
import {ApisService} from '../apis.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-gateway',
  templateUrl: './gateway.component.html',
  styleUrls: ['./gateway.component.css']
})
export class GatewayComponent implements OnInit, OnDestroy {
  gateways = [];
  subscription$ = new Subject();

  constructor(
    private apiService: ApisService
  ) {
  }

  ngOnInit(): void {
    this.getDate();
  }

  getDate(): void {
    this.apiService.getGateways().pipe(takeUntil(this.subscription$)).subscribe((gateways: any) => {
      this.gateways = gateways;
    });
  }

  ngOnDestroy(): void {
    this.subscription$.next();
    this.subscription$.complete();
    // console.log(this.subscription$);
  }

}
