import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {GatewayComponent} from './gateway/gateway.component';
import {DevicesComponent} from './devices/devices.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {ApisService} from './apis.service';
import { AddGatewayComponent } from './add-gateway/add-gateway.component';
import {ReactiveFormsModule} from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: GatewayComponent,
  },
  {
    path: 'gateways',
    component: GatewayComponent,
  },
  {
    path: 'edit-gateway/:id',
    component: DevicesComponent,
  },
  {
    path: 'add-gateway',
    component: AddGatewayComponent,
  },
  {
    path: '**',
    redirectTo: '/',
    pathMatch: 'full'
  }
];


@NgModule({
  declarations: [
    AppComponent,
    GatewayComponent,
    DevicesComponent,
    AddGatewayComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ApisService],
  bootstrap: [AppComponent],
})
export class AppModule {
}
