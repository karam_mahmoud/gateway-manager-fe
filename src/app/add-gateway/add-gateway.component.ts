import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {takeUntil} from 'rxjs/operators';
import {ApisService} from '../apis.service';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-gateway',
  templateUrl: './add-gateway.component.html',
  styleUrls: ['./add-gateway.component.css']
})
export class AddGatewayComponent implements OnInit {
  subscription$ = new Subject();
  gateWayForm = new FormGroup({
    serialNumber: new FormControl('', [Validators.required]),
    humanName: new FormControl('', [Validators.required]),
    ipAddress: new FormControl('', [Validators.required]),
  });

  constructor(
    private apiService: ApisService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  submit(): void {
    // tslint:disable-next-line:forin
    for (const i in this.gateWayForm.controls) {
      this.gateWayForm.get(i).markAsTouched();
    }
    if (this.gateWayForm.valid) {
      this.apiService.addGateway(this.gateWayForm.value).pipe(takeUntil(this.subscription$)).subscribe((gateways: any) => {
        alert('Gateway added successfully');
        this.router.navigate(['gateways']);
      });
    }
  }
}
