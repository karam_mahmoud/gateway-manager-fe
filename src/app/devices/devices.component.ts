import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ApisService} from '../apis.service';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.css']
})
export class DevicesComponent implements OnInit, OnDestroy {
  devices = [];
  subscription$ = new Subject();
  deviceForm = new FormGroup({
    UID: new FormControl('', [Validators.required]),
    vendor: new FormControl('', [Validators.required]),
    status: new FormControl(false, [Validators.required]),
    gatewayId: new FormControl('', [Validators.required]),
  });

  constructor(
    private apiService: ApisService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.subscription$)).subscribe(params => {
      this.deviceForm.get('gatewayId').setValue(params.id);
      this.getDevices(params.id);
    });
  }

  getDevices(gatewayId): void {
    this.apiService.getDevices(gatewayId).pipe(takeUntil(this.subscription$)).subscribe((devices: any) => {
      this.devices = devices;
    });
  }

  removeDevice(id): void {
    this.apiService.removeDevice(id).pipe(takeUntil(this.subscription$)).subscribe(() => {
      this.devices.filter(d => d._id !== id);
      alert('Device removed successfully');
    });
  }

  submit(): void {
    // tslint:disable-next-line:forin
    for (const i in this.deviceForm.controls) {
      this.deviceForm.get(i).markAsTouched();
    }
    if (this.deviceForm.valid) {
      this.apiService.addDevice(this.deviceForm.value).pipe(takeUntil(this.subscription$)).subscribe((gateways: any) => {
        alert('device added successfully');
      });
    }
  }

  ngOnDestroy(): void {
    this.subscription$.next();
    this.subscription$.complete();
    // console.log(this.subscription$);
  }
}
